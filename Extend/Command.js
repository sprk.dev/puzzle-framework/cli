"use strict";

const logger = require("../logger");
const PObject = require("../Base/PObject");

/**
 * Class Command
 *
 * @abstract
 */
class Command extends PObject {
  /**
   *
   * @protected
   *
   * @property {string}
   */
  _command = "";

  /**
   *
   * @protected
   *
   * @property {string}
   */
  _description = "";

  /**
   *
   * @protected
   *
   * @property {string[]}
   */
  _aliases = [];

  /**
   *
   * @protected
   *
   * @property {winston.Logger}
   */
  log = logger;

  /**
   *
   * @protected
   *
   * @param {string} command
   * @param {string} [description]
   * @param {string[]} [aliases]
   */
  constructor(command, description, aliases) {
    super();
    this._command = command;
    this._description = description || "";
    this._aliases = aliases || [];
  }

  /**
   *
   * @return {ICommandObject}
   */
  toObject() {
    return {
      command: this._command,
      aliases: this._aliases,
      desc: this._description,
      describe: this._description,
      builder: (yargs) => this.builder(yargs),
      handler: (argv) => this.run(argv)
    };
  }

  /**
   * @protected
   * @abstract
   *
   * @param {*[]} argv
   */
  run(argv) {
  }

  /**
   * @protected
   *
   * @param {yargs} yargs
   */
  builder(yargs) {
    return {};
  }
}

module.exports = Command;
