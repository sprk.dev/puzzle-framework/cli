"use strict";

const { createLogger, format, transports } = require("winston");


const logger = createLogger({
  level: "silly",
  format: format.combine(
    format.splat(),
    format.cli()
  ),
  transports: [
    new transports.Console({ level: "silly" }),
  ]
});

module.exports = logger;
