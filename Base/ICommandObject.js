"use strict";

/**
 *
 * @typedef {object} ICommandObject
 * @param {string} command
 * @param {string[]} [aliases]
 * @param {string} desc
 * @param {Function} builder
 * @param {Function} handler
 */
