"use strict";

const PObject = require("./PObject");

/**
 * Class CommandLoader
 */
class CommandLoader extends PObject {
  /**
   * @protected
   *
   * @property {Command[]}
   */
  _commands = [];

  /**
   *
   * @property {Command} command
   */
  register(command) {
    this._commands.push(command);
  }

  /**
   *
   * @param {yargs} yargs
   *
   * @return {yargs}
   */
  build(yargs) {
    this._commands.forEach((command) => {
      yargs.command(command.toObject());
    });
    return yargs;
  }
}

module.exports = CommandLoader;
