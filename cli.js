"use strict";

const yargs = require("yargs");
const CommandLoader = require("./Base/CommandLoader");
const pkgInfo = require("./package");

const loader = new CommandLoader();
let { name } = pkgInfo;

let finishCommand = () => {
  process.exit(0);
};

module.exports = {
  Loader: loader,
  set name(newName) {
    name = newName;
  },
  get name() {
    return name;
  },
  set finishCommand(newCommand) {
    finishCommand = newCommand;
  },
  get finishCommand() {
    return finishCommand;
  },
  run() {
    yargs.scriptName(name);
    const yarg = loader.build(yargs);

    return yarg
      .onFinishCommand(finishCommand)
      .demandCommand()
      .showHelpOnFail(false, "Specify --help for available options")
      .help("help")
      .wrap(80)
      .argv;
  }
};
