# Puzzle CLI Framework

Puzzle CLI Framework wants to be a simple to use framework to build CLI applications using a micro-application architecture.

The purpose of this framework is to be as small and lite as possible. It is composed of core elements and
some core modules (called puzzles). The core modules can be enable/disabled through feature switches in the
application that uses the framework.

## Contributing

To contribute please read the [CONTRIBUTING.md](https://gitlab.com/sprk.dev/puzzle-framework/blob/master/CONTRIBUTING.md) file.

